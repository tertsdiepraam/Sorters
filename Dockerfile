FROM rust

RUN apt-get update; \
    apt-get install -y --no-install-recommends nodejs npm; \
    curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh;
