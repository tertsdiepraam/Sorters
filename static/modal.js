var imageModal = document.getElementById("image-modal");
var imageBtn = document.getElementById("open-image-modal");
var imageSpan = document.getElementById("close-image-modal");

imageBtn.onclick = function() {
  imageModal.style.display = "block";
}

imageSpan.onclick = function() {
  imageModal.style.display = "none";
}

window.onclick = function(event) {
  if (event.target == imageModal) {
    imageModal.style.display = "none";
  }
}
