const currentTheme = localStorage.getItem('theme') ? localStorage.getItem('theme') : 'light';
const toggleSwitch = document.getElementById('darkmodetoggle');

function switchTheme(e) {
    if (e.target.checked) {
        document.documentElement.setAttribute('data-theme', 'dark');
        localStorage.setItem('theme', 'dark');
    }
    else {
        document.documentElement.setAttribute('data-theme', 'light');
        localStorage.setItem('theme', 'light');
    }
}

toggleSwitch.addEventListener('change', switchTheme);
if (currentTheme == 'dark') {
    toggleSwitch.checked = true;
};
document.documentElement.setAttribute('data-theme', currentTheme);
