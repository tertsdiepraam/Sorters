class Recorder {
    constructor(canvas) {
        this.recording = false;
        this.canvas = canvas;
        this.stream = canvas.captureStream();
        this.download_url = null;
    }

    toggle() {
        if (this.recording) {
            this.stop();
        } else {
            this.start();
        }
    }

    start() {
        if (this.download_url !== null) {
            URL.revokeObjectURL(this.download_url);
            this.download_url = null;
        }
        this.recorder = new MediaRecorder(this.stream, {
            mimeType: "video/webm; codecs=vp8"
        });

        this.chunks = [];
        let chunks = this.chunks;
        this.recorder.ondataavailable = e => {
            if (e.data && e.data.size > 0) {
                chunks.push(e.data);
            }
        }
        let rec = this;
        this.recorder.onstop = (event) => {
            let blob = new Blob(rec.chunks, {type: "video/webm"});
            rec.download_url = URL.createObjectURL(blob);
            let a = document.getElementById('download-anchor')
            a.href = rec.url();
            a.download = 'sorters_export.webm';
            a.click();
        }
        this.recorder.start(100);
        this.recording = true;
    }

    requestFrame() {
        this.stream.requestFrame();
    }

    stop() {
        this.recorder.stop();
        this.recording = false;
    }

    url() {
        return this.download_url;
    }
}

let recorder = null;

let canvas = document.getElementById('export-canvas')
let width = document.getElementById('export-width');
let height = document.getElementById('export-height');
let start = document.getElementById('start-export-video');
let stop = document.getElementById('stop-export-video');

start.addEventListener('click', () => {
    canvas.width = width.value;
    canvas.height = height.value;
    if (recorder === null) {
        recorder = new Recorder(canvas);
    }
    recorder.start();
    start.style.display = 'none';
    stop.style.display = '';
});

stop.addEventListener('click', () => {
    recorder.stop();
    start.style.display = '';
    stop.style.display = 'none';
})

function get_recording() {
    return recorder !== null && recorder.recording;
}
