mod colorscheme;
mod render;
mod sort;
mod vecinit;
mod vecviz;
mod webtools;
use std::{cell::RefCell, f64, rc::Rc};
use js_sys::JsString;
use vecviz::VecViz;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::{CanvasRenderingContext2d, HtmlCanvasElement};
use webtools::get_canvas_and_context;
use webtools::{
    get_button, get_colorscheme_select, get_dark_mode, get_dark_mode_input, get_download_anchor,
    get_input, get_playback_speed, get_playback_speed_input, get_recording,
    get_visualization_select, request_animation_frame, resize_canvas, window,
};
type RcVecViz = Rc<RefCell<VecViz>>;
type RcCanvas = Rc<RefCell<HtmlCanvasElement>>;
type RcContext = Rc<RefCell<CanvasRenderingContext2d>>;

#[wasm_bindgen(start)]
pub fn main_js() -> Result<(), JsValue> {
    #[cfg(feature = "console_error_panic_hook")]
    console_error_panic_hook::set_once();

    console_log::init().unwrap();

    let (canvas, context) = get_canvas_and_context("canvas");
    let (export_canvas, export_context) = get_canvas_and_context("export-canvas");

    let canvas_rc = Rc::new(RefCell::new(canvas));
    let context_rc = Rc::new(RefCell::new(context));
    let export_canvas_rc = Rc::new(RefCell::new(export_canvas));
    let export_context_rc = Rc::new(RefCell::new(export_context));
    let vv_rc = Rc::new(RefCell::new(VecViz::new(300, 300)));

    {
        let mut vv = vv_rc.try_borrow_mut().unwrap();
        let canvas = canvas_rc.borrow();
        let context = context_rc.borrow();
        resize_canvas(&canvas);
        context.clear_rect(0.0, 0.0, canvas.width() as f64, canvas.height() as f64);
        vv.init();
        vv.render(&canvas, &context, get_dark_mode(), false);
    }

    change_visualization(vv_rc.clone());

    start_animation_loop(
        vv_rc.clone(),
        canvas_rc.clone(),
        context_rc.clone(),
        export_canvas_rc.clone(),
        export_context_rc.clone(),
    );
    set_canvas_resize(vv_rc.clone(), canvas_rc.clone(), context_rc.clone());
    set_colorscheme(vv_rc.clone(), canvas_rc.clone(), context_rc.clone());
    set_visualization(vv_rc.clone(), canvas_rc.clone(), context_rc.clone());
    set_dark_mode(vv_rc.clone(), canvas_rc.clone(), context_rc.clone());
    set_export_image(
        vv_rc.clone(),
        export_canvas_rc.clone(),
        export_context_rc.clone(),
    );
    set_interface_interactions(vv_rc);
    Ok(())
}

fn start_animation_loop(
    vv_rc: RcVecViz,
    canvas_rc: RcCanvas,
    context_rc: RcContext,
    export_canvas_rc: RcCanvas,
    export_context_rc: RcContext,
) {
    let f = Rc::new(RefCell::new(None));
    let g = f.clone();

    *g.borrow_mut() = Some(Closure::wrap(Box::new(move || {
        let mut vv = match vv_rc.try_borrow_mut() {
            Ok(vv) => vv,
            Err(_) => return,
        };
        if vv.state.running {
            vv.tick();
            let dark = get_dark_mode();
            let recording = get_recording();

            let canvas = canvas_rc.borrow();
            let context = context_rc.borrow();
            vv.render(&canvas, &context, dark, false);

            if recording {
                let export_canvas = export_canvas_rc.borrow();
                let export_context = export_context_rc.borrow();
                vv.render(&export_canvas, &export_context, dark, true);
            }
        }
        request_animation_frame(f.borrow().as_ref().unwrap())
    }) as Box<dyn FnMut()>));
    request_animation_frame(g.borrow().as_ref().unwrap());
}

fn set_canvas_resize(vv_rc: RcVecViz, canvas_rc: RcCanvas, context_rc: RcContext) {
    let a = Closure::wrap(Box::new(move || {
        let canvas = canvas_rc.borrow();
        let context = context_rc.borrow();
        let mut vv = match vv_rc.try_borrow_mut() {
            Ok(vv) => vv,
            Err(_) => return,
        };
        resize_canvas(&canvas);
        vv.state.full_redraw = true;
        if !vv.state.running {
            context.clear_rect(0.0, 0.0, canvas.width() as f64, canvas.height() as f64);
            vv.render(&canvas, &context, get_dark_mode(), false)
        }
    }) as Box<dyn FnMut()>);
    window().set_onresize(Some(a.as_ref().unchecked_ref()));
    a.forget();
}

fn set_interface_interactions(vv_rc: RcVecViz) {
    set_play_forwards(vv_rc.clone());
    set_play_backwards(vv_rc.clone());
    set_step_forwards(vv_rc.clone());
    set_step_backwards(vv_rc.clone());
    set_pause(vv_rc.clone());
    set_restart(vv_rc.clone());
    set_playback_speed(vv_rc.clone());
}

fn set_play_forwards(vv_rc: RcVecViz) {
    let a = Closure::wrap(Box::new(move || {
        let mut vv = vv_rc.try_borrow_mut().unwrap();
        vv.play_forwards();
    }) as Box<dyn FnMut()>);
    get_button("play-forwards").set_onclick(Some(a.as_ref().unchecked_ref()));
    a.forget();
}

fn set_play_backwards(vv_rc: RcVecViz) {
    let a = Closure::wrap(Box::new(move || {
        let mut vv = vv_rc.try_borrow_mut().unwrap();
        vv.play_backwards();
    }) as Box<dyn FnMut()>);
    get_button("play-backwards").set_onclick(Some(a.as_ref().unchecked_ref()));
    a.forget();
}

fn set_step_forwards(vv_rc: RcVecViz) {
    let a = Closure::wrap(Box::new(move || {
        let mut vv = vv_rc.try_borrow_mut().unwrap();
        vv.step_forwards();
    }) as Box<dyn FnMut()>);
    get_button("step-forwards").set_onclick(Some(a.as_ref().unchecked_ref()));
    a.forget();
}

fn set_step_backwards(vv_rc: RcVecViz) {
    let a = Closure::wrap(Box::new(move || {
        let mut vv = vv_rc.try_borrow_mut().unwrap();
        vv.step_backwards();
    }) as Box<dyn FnMut()>);
    get_button("step-backwards").set_onclick(Some(a.as_ref().unchecked_ref()));
    a.forget();
}

fn set_pause(vv_rc: RcVecViz) {
    let a = Closure::wrap(Box::new(move || {
        let mut vv = vv_rc.try_borrow_mut().unwrap();
        vv.pause();
    }) as Box<dyn FnMut()>);
    get_button("pause").set_onclick(Some(a.as_ref().unchecked_ref()));
    a.forget();
}

fn set_restart(vv_rc: RcVecViz) {
    let a = Closure::wrap(Box::new(move || {
        let mut vv = vv_rc.try_borrow_mut().unwrap();
        vv.init();
    }) as Box<dyn FnMut()>);
    get_button("restart").set_onclick(Some(a.as_ref().unchecked_ref()));
    a.forget();
}

fn set_playback_speed(vv_rc: RcVecViz) {
    let a = Closure::wrap(Box::new(move || {
        let mut vv = vv_rc.try_borrow_mut().unwrap();
        if let Ok(speed) = get_playback_speed() {
            vv.state.speed = speed
        }
    }) as Box<dyn FnMut()>);
    get_playback_speed_input().set_onchange(Some(a.as_ref().unchecked_ref()));
    a.forget();
}

fn set_colorscheme(vv_rc: RcVecViz, canvas_rc: RcCanvas, context_rc: RcContext) {
    let a = Closure::wrap(Box::new(move || {
        let mut vv = vv_rc.try_borrow_mut().unwrap();
        vv.colorscheme = colorscheme::get_colorscheme(get_colorscheme_select().value());
        vv.state.full_redraw = true;
        if !vv.state.running {
            let canvas = canvas_rc.borrow();
            let context = context_rc.borrow();
            context.clear_rect(0.0, 0.0, canvas.width() as f64, canvas.height() as f64);
            vv.render(&canvas, &context, get_dark_mode(), false);
        }
    }) as Box<dyn FnMut()>);
    get_colorscheme_select().set_onchange(Some(a.as_ref().unchecked_ref()));
    a.forget();
}

fn change_visualization(vv_rc: RcVecViz) {
    let mut vv = vv_rc.try_borrow_mut().unwrap();
    vv.visualization =
        render::get_visualization(vv.state.len as usize, get_visualization_select().value());

    use render::Visualization::*;
    let disabled = match vv.visualization {
        Animated(_) => false,
        Static(_) => true,
    };
    get_button("play-backwards").set_disabled(disabled);
    get_button("play-forwards").set_disabled(disabled);
    get_button("pause").set_disabled(disabled);
    get_button("step-forwards").set_disabled(disabled);
    get_button("step-backwards").set_disabled(disabled);
    get_button("start-export-video").set_disabled(disabled);
    get_input("speed").set_disabled(disabled);
}

fn set_visualization(vv_rc: RcVecViz, canvas_rc: RcCanvas, context_rc: RcContext) {
    let a = Closure::wrap(Box::new(move || {
        change_visualization(vv_rc.clone());
        let mut vv = vv_rc.try_borrow_mut().unwrap();
        vv.state.full_redraw = true;
        if !vv.state.running {
            let canvas = canvas_rc.borrow();
            let context = context_rc.borrow();
            context.clear_rect(0.0, 0.0, canvas.width() as f64, canvas.height() as f64);
            vv.render(&canvas, &context, get_dark_mode(), false);
        }
    }) as Box<dyn FnMut()>);
    get_visualization_select().set_onchange(Some(a.as_ref().unchecked_ref()));
    a.forget();
}

fn set_dark_mode(vv_rc: RcVecViz, canvas_rc: RcCanvas, context_rc: RcContext) {
    let a = Closure::wrap(Box::new(move || {
        let mut vv = vv_rc.try_borrow_mut().unwrap();
        if !vv.state.running {
            let canvas = canvas_rc.borrow();
            let context = context_rc.borrow();
            context.clear_rect(0.0, 0.0, canvas.width() as f64, canvas.height() as f64);
            vv.render(&canvas, &context, get_dark_mode(), false);
        }
    }) as Box<dyn FnMut()>);
    get_dark_mode_input().set_onchange(Some(a.as_ref().unchecked_ref()));
    a.forget();
}

fn set_export_image(vv_rc: RcVecViz, canvas_rc: RcCanvas, context_rc: RcContext) {
    let a = Closure::wrap(Box::new(move || {
        let mut vv = vv_rc.try_borrow_mut().unwrap();
        let canvas = canvas_rc.borrow();
        let context = context_rc.borrow();

        let width = get_input("export-width").value().parse().unwrap();
        let height = get_input("export-height").value().parse().unwrap();
        canvas.set_width(width);
        canvas.set_height(height);

        context.clear_rect(0.0, 0.0, width as f64, height as f64);
        vv.render(&canvas, &context, get_dark_mode(), false);

        let a = get_download_anchor();
        a.set_href(&canvas.to_data_url_with_type("image/png").unwrap());
        a.set_download("sorters_export");
        a.click();
    }) as Box<dyn FnMut()>);
    get_button("export-image").set_onclick(Some(a.as_ref().unchecked_ref()));
    a.forget();
}
