use crate::vecviz::History;
use crate::vecviz::{HistoryElement, AnimationState};
use crate::colorscheme::ColorScheme;
use js_sys::JsString;
use std::collections::HashMap;
use std::f64::consts::PI;
use web_sys::{CanvasRenderingContext2d, HtmlCanvasElement};

pub enum Visualization {
    Animated(Box<dyn AnimatedVisualization>),
    Static(Box<dyn StaticVisualization>),
}

pub trait AnimatedVisualization {
    fn render(
        &mut self,
        state: &mut AnimationState,
        cs: &Box<dyn ColorScheme>,
        canvas: &HtmlCanvasElement,
        ctx: &CanvasRenderingContext2d,
        dark: bool,
        full_redraw: bool
    );
}

pub trait StaticVisualization {
    fn render(
        &self,
        state: &AnimationState,
        history: &History,
        cs: &Box<dyn ColorScheme>,
        canvas: &HtmlCanvasElement,
        ctx: &CanvasRenderingContext2d,
        dark: bool,
    );
}

pub fn get_visualization(_len: usize, s: String) -> Visualization {
    use Visualization::*;
    match s.as_ref() {
        "bars" => Animated(Box::new(Bars {})),
        "dots" => Animated(Box::new(Dots {})),
        "sticks" => Animated(Box::new(Sticks {})),
        "tubes" => Static(Box::new(Tubes {})),
        "arcs" => Static(Box::new(Arcs {})),
        _ => Animated(Box::new(Bars {})),
    }
}

struct Bars;

impl AnimatedVisualization for Bars {
    fn render(
        &mut self,
        state: &mut AnimationState,
        cs: &Box<dyn ColorScheme>,
        canvas: &HtmlCanvasElement,
        ctx: &CanvasRenderingContext2d,
        dark: bool,
        full_redraw: bool,
    ) {
        let w: f64 = (canvas.width() as f64) / (state.len as f64);
        let max_h: f64 = canvas.height() as f64;

        let draw_bar = |i, n, max, style| {
            ctx.set_fill_style(&JsString::from(style));
            let h: f64 = ((n + 1) as f64) / max * max_h;
            let x = ((i as f64) * w).floor();
            ctx.clear_rect(x, 0.0, w.ceil(), max_h);
            ctx.fill_rect(x, max_h, w.ceil(), -h);
        };

        for (i, n) in state.current_vec.iter().enumerate() {
            if state.changed[i] || full_redraw {
                draw_bar(i, *n, state.max as f64, cs.foreground(*n, state.max, dark));
                state.changed[i] = false;
            }
        }

        let (a, b) = state.highlighted_indices;
        if let Some(i) = a {
            let n = state.current_vec[i];
            draw_bar(i, n, state.max as f64, cs.highlight(n, state.max, dark));
            state.changed[i] = true;
        }
        if let Some(i) = b {
            let n = state.current_vec[i];
            draw_bar(i, n, state.max as f64, cs.highlight(n, state.max, dark));
            state.changed[i] = true;
        }
    }
}

struct Dots;

impl AnimatedVisualization for Dots {
    fn render(
        &mut self,
        state: &mut AnimationState,
        cs: &Box<dyn ColorScheme>,
        canvas: &HtmlCanvasElement,
        ctx: &CanvasRenderingContext2d,
        dark: bool,
        _full_redraw: bool
    ) {
        ctx.clear_rect(0.0, 0.0, canvas.width() as f64, canvas.height() as f64);
        let dotsize = (canvas.width() as f64 / (state.len - 1) as f64)
            .powf(2.0 / 3.0)
            .ceil();
        let margin = dotsize + 10.0;
        let w: f64 = ((canvas.width() as f64) - 2.0 * margin) / ((state.len - 1) as f64);
        let max_h: f64 = canvas.height() as f64 - 2.0 * margin;

        let draw_dot = |i, n, style| {
            ctx.set_fill_style(&JsString::from(style));
            let h: f64 = (n as f64) / ((state.max - 1) as f64) * max_h;
            let x = ((i as f64) * w).floor() + margin;
            ctx.begin_path();
            ctx.ellipse(x, max_h - h + margin, dotsize, dotsize, 0.0, 0.0, 2.0 * PI)
                .unwrap();
            ctx.fill();
        };

        for (i, n) in state.current_vec.iter().enumerate() {
            draw_dot(i, *n, cs.foreground(*n, state.max, dark));
        }

        let (a, b) = state.highlighted_indices;
        if let Some(i) = a {
            let n = state.current_vec[i];
            draw_dot(i, n, cs.highlight(n, state.max, dark));
        }
        if let Some(i) = b {
            let n = state.current_vec[i];
            draw_dot(i, n, cs.highlight(n, state.max, dark));
        }
    }
}

struct Sticks;

impl AnimatedVisualization for Sticks {
    fn render(
        &mut self,
        state: &mut AnimationState,
        cs: &Box<dyn ColorScheme>,
        canvas: &HtmlCanvasElement,
        ctx: &CanvasRenderingContext2d,
        dark: bool,
        _full_redraw: bool,
    ) {
        ctx.clear_rect(0.0, 0.0, canvas.width() as f64, canvas.height() as f64);
        let stick_length = 200.0;
        let max_angle = PI / 5.0;
        let margin = 20.0;

        let rest_angle = PI - 2.0 * max_angle;
        let min_x = max_angle.cos() * stick_length + margin;
        let width = canvas.width() as f64 - 2.0 * min_x;

        let baseline = (canvas.height() as f64 + stick_length) / 2.0;
        let len_f = state.len as f64 - 1.0;
        let max_f = state.max as f64 - 1.0;

        ctx.set_line_width((width / len_f).powf(2.0 / 3.0).ceil());
        ctx.set_line_cap("round");

        let draw_sticks = |i, n, style| {
            ctx.set_stroke_style(&JsString::from(style));
            ctx.begin_path();
            let x = min_x + i as f64 / len_f * width;
            ctx.move_to(x, baseline);
            let angle = max_angle + rest_angle * (n as f64) / max_f;
            ctx.line_to(
                x - stick_length * angle.cos(),
                baseline - stick_length * angle.sin(),
            );
            ctx.stroke();
        };

        for (i, n) in state.current_vec.iter().enumerate() {
            draw_sticks(i, *n, cs.foreground(*n, state.max, dark));
        }

        let (a, b) = state.highlighted_indices;
        if let Some(i) = a {
            let n = state.current_vec[i];
            draw_sticks(i, n, cs.highlight(n, state.max, dark));
        }
        if let Some(i) = b {
            let n = state.current_vec[i];
            draw_sticks(i, n, cs.highlight(n, state.max, dark));
        }
    }
}

struct Tubes;

impl StaticVisualization for Tubes {
    fn render(
        &self,
        state: &AnimationState,
        history: &History,
        cs: &Box<dyn ColorScheme>,
        canvas: &HtmlCanvasElement,
        ctx: &CanvasRenderingContext2d,
        dark: bool,
    ) {
        let width = canvas.width() as f64;
        let height = canvas.height() as f64;

        let x_max = width;

        let line_space = height / state.len as f64;
        let min_y = line_space / 2.0;
        let line_width = line_space.powf(0.8);
        ctx.set_line_width(line_width.ceil());

        let mut paths: Vec<Vec<(usize, usize)>> = Vec::new();
        for i in 0..state.current_vec.len() {
            paths.push(Vec::new());
            paths[i].push((0, i));
        }

        let mut i = 0;
        for change in history.operations.iter() {
            if let HistoryElement::Swap(j, k) = change {
                let j = *j;
                let k = *k;
                paths.swap(j, k);
                paths[j].push((i, j));
                paths[k].push((i, k));
                i += 1;
            }
        }
        let limit = i as f64;

        // Horizontal lines
        ctx.set_line_cap("round");
        for (i, path) in paths.iter().enumerate() {
            let n = history.end[i];
            ctx.begin_path();
            ctx.set_stroke_style(&JsString::from(cs.foreground(n, state.max, dark)));
            let mut iter = path.iter();
            let (x0, y0) = iter.next().unwrap();
            let mut prev_y = *y0 as f64 * line_space + min_y;
            ctx.move_to(*x0 as f64 * width / limit, prev_y);
            for (x, y) in iter {
                ctx.line_to(*x as f64 * width / limit, prev_y);
                prev_y = *y as f64 * line_space + min_y;
                ctx.move_to((*x as f64 + 1.0) * width / limit, prev_y)
            }
            ctx.line_to(x_max, i as f64 * line_space + min_y);
            ctx.stroke();
        }

        // Diagonals
        ctx.set_line_cap("round");
        for (i, path) in paths.iter().enumerate() {
            let n = history.end[i];
            ctx.begin_path();
            ctx.set_stroke_style(&JsString::from(cs.foreground(n, state.max, dark)));
            let mut iter = path.iter();
            let (_x0, y0) = iter.next().unwrap();
            let mut prev_y = *y0 as f64 * line_space + min_y;
            for (x, y) in iter {
                let x = *x as f64;
                let y = *y as f64;
                ctx.move_to(x * width / limit, prev_y);
                prev_y = y * line_space + min_y;
                ctx.line_to((x + 1.0) * width / limit, prev_y);
            }
            ctx.line_to(x_max, i as f64 * line_space + min_y);
            ctx.stroke();
        }
    }
}

struct Arcs;

impl StaticVisualization for Arcs {
    fn render(
        &self,
        state: &AnimationState,
        history: &History,
        cs: &Box<dyn ColorScheme>,
        canvas: &HtmlCanvasElement,
        ctx: &CanvasRenderingContext2d,
        dark: bool,
    ) {
        let point_space = canvas.width() as f64 / state.len as f64;
        let min_x = point_space / 2.0;
        let y = canvas.height() as f64 / 2.0;

        let mut map = HashMap::new();
        for elem in &history.operations {
            if let HistoryElement::Swap(i, j) = elem {
                map.insert(
                    (i, j),
                    match map.get(&(i, j)) {
                        Some(n) => n + 1,
                        None => 1,
                    },
                );
            }
        }

        let mut max = 0;
        for n in map.values() {
            if *n > max {
                max = *n;
            }
        }

        for ((i, j), n) in map {
            ctx.begin_path();
            ctx.set_stroke_style(&JsString::from(cs.foreground(n, max, dark)));
            let x = (*i as f64 + *j as f64) / 2.0 * point_space + min_x;
            let radius = (*i as f64 - *j as f64).abs() * point_space / 2.0;
            ctx.arc(x, y, radius, 0.0, 2.0 * PI).unwrap();
            ctx.stroke();
        }
    }
}
