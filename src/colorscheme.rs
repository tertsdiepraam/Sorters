pub trait ColorScheme {
    fn foreground(&self, n: u32, max: u32, dark: bool) -> String;

    fn highlight(&self, n: u32, max: u32, dark: bool) -> String;
}

pub fn get_colorscheme(s: String) -> Box<dyn ColorScheme>{
    match s.as_ref() {
        "simple" => Box::new(Simple{}),
        "rainbow" => Box::new(Rainbow{}),
        "viridis" => Box::new(Viridis{}),
        "inferno" => Box::new(Inferno{}),
        "plasma" => Box::new(Plasma{}),
        _ => Box::new(Simple{}),
    }
}

struct Simple;

impl ColorScheme for Simple {
    fn foreground(&self, _n: u32, _max: u32, dark: bool) -> String {
        String::from(if dark {
            "white"
        } else {
            "black"
        })
    }

    fn highlight(&self, _n: u32, _max: u32, _dark: bool) -> String {
        String::from("red")
    }
}

struct Rainbow;

impl ColorScheme for Rainbow {
    fn foreground(&self, n: u32, max: u32, _dark: bool) -> String {
        format!("hsl({}, 100%, 50%)", n*300/max)
    }

    fn highlight(&self, _n: u32, _max: u32, dark: bool) -> String {
        String::from(if dark {
            "white"
        } else {
            "black"
        })
    }
}

struct Viridis;

impl ColorScheme for Viridis {
    fn foreground(&self, n:u32, max: u32, _dark: bool) -> String {
        format!("#{:x}", colorous::VIRIDIS.eval_rational(n as usize, max as usize))
    }

    fn highlight(&self, _n: u32, _max: u32, _dark: bool) -> String {
        String::from("red")
    }
}

struct Inferno;

impl ColorScheme for Inferno {
    fn foreground(&self, n:u32, max: u32, _dark: bool) -> String {
        format!("#{:x}", colorous::INFERNO.eval_rational(n as usize, max as usize))
    }

    fn highlight(&self, _n: u32, _max: u32, _dark: bool) -> String {
        String::from("#40FF40")
    }
}


struct Plasma;

impl ColorScheme for Plasma {
    fn foreground(&self, n:u32, max: u32, _dark: bool) -> String {
        format!("#{:x}", colorous::PLASMA.eval_rational(n as usize, max as usize))
    }

    fn highlight(&self, _n: u32, _max: u32, _dark: bool) -> String {
        String::from("#40FF40")
    }
}
