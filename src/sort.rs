use crate::vecviz::History;
use std::cmp::Ordering::Greater;

pub fn apply_sort(history: &mut History, algorithm: String) {
    (match algorithm.as_ref() {
        "insertion sort" => insertion_sort,
        "selection sort" => selection_sort,
        "quicksort" => quicksort,
        "bubble sort" => bubble_sort,
        "gnome sort" => gnome_sort,
        "cocktail shaker sort" => cocktail_shaker_sort,
        "shellsort" => shellsort,
        "comb sort" => comb_sort,
        "odd-even sort" => odd_even_sort,
        _ => insertion_sort,
    })(history)
}

fn insertion_sort(history: &mut History) {
    let mut j;
    for i in 1..history.start.len() {
        j = i;
        while j > 0 && history.cmp(j - 1, j) == Greater {
            history.swap(j - 1, j);
            j -= 1;
        }
    }
}

fn selection_sort(history: &mut History) {
    for i in 0..history.start.len() {
        let mut min_j = i;
        for j in i..history.start.len() {
            if history.cmp(min_j, j) == Greater {
                min_j = j
            }
        }

        if min_j != i {
            history.swap(i, min_j);
        }
    }
}

fn gnome_sort(history: &mut History) {
    let mut pos = 0;
    while pos < history.start.len() {
        if pos == 0 || history.cmp(pos - 1, pos) != Greater {
            pos += 1;
        } else {
            history.swap(pos, pos - 1);
            pos -= 1;
        }
    }
}

fn cocktail_shaker_sort(history: &mut History) {
    let mut swapped = true;
    while swapped {
        swapped = false;
        for i in 0..history.start.len() - 1 {
            if history.cmp(i, i + 1) == Greater {
                history.swap(i, i + 1);
                swapped = true;
            }
        }
        if !swapped {
            break;
        }
        for i in (1..history.start.len() - 2).rev() {
            if history.cmp(i, i + 1) == Greater {
                history.swap(i, i + 1);
                swapped = true;
            }
        }
    }
}

fn quicksort(mut history: &mut History) {
    fn partition(history: &mut History, lo: usize, hi: usize) -> usize {
        let mut i = lo;
        let mut j = hi;
        let mut pivot = (lo + hi) / 2;
        loop {
            while history.cmp(pivot, i) == Greater {
                i += 1;
            }
            while history.cmp(j, pivot) == Greater {
                j -= 1;
            }
            if i > j {
                return i;
            }
            if pivot == i {
                pivot = j;
            } else if pivot == j {
                pivot = i;
            }
            history.swap(i, j);
            i += 1;
            if j > 0 {
                j -= 1;
            }
        }
    }

    fn quicksort_(history: &mut History, lo: usize, hi: usize) {
        if lo < hi {
            let p = partition(history, lo, hi);
            quicksort_(history, lo, p - 1);
            quicksort_(history, p, hi);
        }
    }

    let len = history.start.len();
    quicksort_(&mut history, 0, len - 1);
}

pub fn bubble_sort(history: &mut History) {
    for j in (0..history.start.len()).rev() {
        for i in 0..j {
            if history.cmp(i, i + 1) == Greater {
                history.swap(i, i + 1);
            }
        }
    }
}

pub fn shellsort(history: &mut History) {
    let gaps = [701, 301, 132, 57, 23, 10, 4, 1];
    let n = history.start.len();
    for &gap in gaps.iter() {
        for i in gap..n {
            let mut j = i;
            while j >= gap && history.cmp(j - gap, j) == Greater {
                history.swap(j - gap, j);
                j -= gap;
            }
        }
    }
}

pub fn comb_sort(history: &mut History) {
    let mut gap = history.start.len();
    let shrink = 1.3;
    let mut sorted = false;

    while !sorted {
        gap = (gap as f64 / shrink) as usize;
        if gap <= 1 {
            gap = 1;
            sorted = true;
        }

        for i in 0..history.start.len() - gap {
            if history.cmp(i, i + gap) == Greater {
                history.swap(i, i + gap);
                sorted = false;
            }
        }
    }
}

pub fn odd_even_sort(history: &mut History) {
    let mut sorted = false;
    while !sorted {
        sorted = true;
        for i in (1..history.start.len() - 1).step_by(2) {
            if history.cmp(i, i + 1) == Greater {
                history.swap(i, i + 1);
                sorted = false;
            }
        }
        for i in (0..history.start.len() - 1).step_by(2) {
            if history.cmp(i, i + 1) == Greater {
                history.swap(i, i + 1);
                sorted = false;
            }
        }
    }
}
