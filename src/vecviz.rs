use crate::render::Visualization;
use crate::webtools::{
    get_algorithm, get_colorscheme_select, get_initialization, get_number_of_elements,
    get_playback_speed, get_visualization_select,
};
use crate::{colorscheme, render, sort, vecinit};
use std::cmp::Ordering;
use js_sys::JsString;
use web_sys::{CanvasRenderingContext2d, HtmlCanvasElement};

#[derive(Clone, Copy)]
pub enum Direction {
    Backwards,
    Forwards,
}

#[derive(Clone, Copy)]
pub enum HistoryElement {
    Swap(usize, usize),
    Compare(usize, usize),
}

use HistoryElement::*;

pub struct History {
    pub operations: Vec<HistoryElement>,
    pub start: Vec<u32>,
    pub end: Vec<u32>,
    pub swaps: u32,
    pub compares: u32,
}

impl History {
    fn new(initialization: String, max: u32, len: u32) -> Self {
        let vec = vecinit::get_initial_vec(initialization, max, len);
        Self {
            operations: Vec::new(),
            start: vec.clone(),
            end: vec,
            swaps: 0,
            compares: 0,
        }
    }

    pub fn swap(&mut self, i: usize, j: usize) {
        self.end.swap(i, j);
        self.operations.push(Swap(i, j));
        self.swaps += 1;
    }

    pub fn cmp(&mut self, i: usize, j: usize) -> Ordering {
        self.operations.push(Compare(i, j));
        self.compares += 1;
        self.end[i].cmp(&self.end[j])
    }
}

use Direction::*;

pub struct AnimationState {
    pub max: u32,
    pub len: usize,
    pub current_vec: Vec<u32>,
    pub highlighted_indices: (Option<usize>, Option<usize>),
    pub changed: Vec<bool>,
    pub current_step: usize,
    pub direction: Direction,
    pub speed: u32,
    pub running: bool,
    pub done: bool,
    pub full_redraw: bool,
}

impl AnimationState {
    fn new(max: u32, len: usize) -> Self {
        Self {
            max,
            len,
            current_vec: Vec::new(),
            highlighted_indices: (None, None),
            changed: Vec::new(),
            current_step: 0,
            direction: Forwards,
            speed: 1,
            running: true,
            done: false,
            full_redraw: true,
        }
    }
}

pub struct VecViz {
    pub state: AnimationState,
    pub history: History,
    pub visualization: Visualization,
    pub colorscheme: Box<dyn colorscheme::ColorScheme>,
}

impl<'a> VecViz {
    pub fn new(max: u32, len: usize) -> VecViz {
        VecViz {
            state: AnimationState::new(max, len),
            history: History::new(String::from("ascending"), max, len as u32),
            visualization: render::get_visualization(len as usize, String::from("bar")),
            colorscheme: colorscheme::get_colorscheme(String::from("rainbow")),
        }
    }

    pub fn init(&mut self) {
        self.state.len = get_number_of_elements() as usize;
        self.state.max = self.state.len as u32;
        self.state.changed = vec![false; self.state.len];
        self.state.full_redraw = true;
        self.history = History::new(get_initialization(), self.state.max, self.state.len as u32);
        self.apply_sort(get_algorithm());
        self.colorscheme = colorscheme::get_colorscheme(get_colorscheme_select().value());
        self.visualization =
            render::get_visualization(self.state.len as usize, get_visualization_select().value());
        self.state.current_step = 0;
        if let Ok(speed) = get_playback_speed() {
            self.state.speed = speed;
        }
        self.state.done = false;
        self.state.running = true;
    }

    fn apply_sort(&mut self, algorithm: String) {
        sort::apply_sort(&mut self.history, algorithm);
        self.state.current_vec = self.history.start.clone();
    }

    pub fn tick(&mut self) {
        for _ in 0..self.state.speed {
            self.step_in_dir(self.state.direction);
        }
    }

    pub fn step_forwards(&mut self) {
        self.step_in_dir(Forwards);
    }

    pub fn step_backwards(&mut self) {
        self.step_in_dir(Backwards);
    }

    fn step_in_dir(&mut self, dir: Direction) {
        let limit = match dir {
            Forwards => self.history.operations.len(),
            Backwards => 0,
        };
        if self.state.current_step == limit {
            self.state.done = true;
            self.state.running = false;
            self.state.highlighted_indices = (None, None);
            return;
        }

        let step = self.state.current_step
            - match dir {
                Backwards => 1,
                Forwards => 0,
            };

        let (i, j) = match self.history.operations[step] {
            Swap(i, j) => {
                self.state.current_vec.swap(i, j);
                (i, j)
            }
            Compare(i, j) => {
                self.state.highlighted_indices = (Some(i), Some(j));
                (i, j)
            }
        };
        self.state.changed[i] = true;
        self.state.changed[j] = true;

        match dir {
            Forwards => self.state.current_step += 1,
            Backwards => self.state.current_step -= 1,
        };
    }

    pub fn render(
        &mut self,
        canvas: &HtmlCanvasElement,
        ctx: &CanvasRenderingContext2d,
        dark: bool,
        export: bool,
    ) {
        use Visualization::*;
        let full_redraw = self.state.full_redraw;
        if export {
            ctx.set_fill_style(&JsString::from(if dark {"#000000"} else {"#FFFFFF"}));
            ctx.fill_rect(0.0, 0.0, canvas.width() as f64, canvas.height() as f64);
        } else if full_redraw {
            ctx.clear_rect(0.0, 0.0, canvas.width() as f64, canvas.height() as f64);
        }
        match &mut self.visualization {
            Animated(v) => v.render(
                &mut self.state,
                &self.colorscheme,
                canvas,
                ctx,
                dark,
                export || full_redraw,
            ),
            Static(v) => v.render(
                &self.state,
                &self.history,
                &self.colorscheme,
                canvas,
                ctx,
                dark,
            ),
        };
        if !export {
            self.state.full_redraw = false;
        }
    }

    pub fn play_forwards(&mut self) {
        if let Visualization::Animated(_) = self.visualization {
            self.state.running = true;
            self.state.direction = Forwards;
        }
    }

    pub fn play_backwards(&mut self) {
        if let Visualization::Animated(_) = self.visualization {
            self.state.running = true;
            self.state.direction = Backwards;
        }
    }

    pub fn pause(&mut self) {
        self.state.running = false;
    }
}
