use std::{convert::TryInto, num::ParseIntError};
use wasm_bindgen::{JsCast, prelude::*};
use web_sys::{
    CanvasRenderingContext2d, Document, Element, HtmlAnchorElement, HtmlButtonElement,
    HtmlCanvasElement, HtmlInputElement, HtmlSelectElement, Window,
};

pub fn request_animation_frame(f: &Closure<dyn FnMut()>) {
    web_sys::window()
        .unwrap()
        .request_animation_frame(f.as_ref().unchecked_ref())
        .unwrap();
}

pub fn resize_canvas(canvas: &HtmlCanvasElement) {
    canvas.set_width(0);
    canvas.set_height(0);
    let width: u32 = canvas.client_width().try_into().unwrap();
    let height: u32 = canvas.client_height().try_into().unwrap();
    canvas.set_width(width);
    canvas.set_height(height);
}

pub fn window() -> Window {
    web_sys::window().unwrap()
}

pub fn document() -> Document {
    window().document().unwrap()
}

pub fn get_element_by_id(id: &str) -> Option<Element> {
    document().get_element_by_id(id)
}

pub fn get_input(id: &str) -> HtmlInputElement {
    get_element_by_id(id)
        .unwrap()
        .dyn_into::<HtmlInputElement>()
        .map_err(|_| ())
        .unwrap()
}

pub fn get_algorithm() -> String {
    get_element_by_id("algorithm")
        .unwrap()
        .dyn_into::<HtmlSelectElement>()
        .map_err(|_| ())
        .unwrap()
        .value()
}

pub fn get_colorscheme_select() -> HtmlSelectElement {
    get_element_by_id("colorscheme")
        .unwrap()
        .dyn_into::<HtmlSelectElement>()
        .map_err(|_| ())
        .unwrap()
}

pub fn get_visualization_select() -> HtmlSelectElement {
    get_element_by_id("visualization")
        .unwrap()
        .dyn_into::<HtmlSelectElement>()
        .map_err(|_| ())
        .unwrap()
}

pub fn get_initialization() -> String {
    get_element_by_id("initialization")
        .unwrap()
        .dyn_into::<HtmlSelectElement>()
        .map_err(|_| ())
        .unwrap()
        .value()
}

pub fn get_button(id: &str) -> HtmlButtonElement {
    get_element_by_id(id)
        .unwrap()
        .dyn_into::<HtmlButtonElement>()
        .map_err(|_| ())
        .unwrap()
}

pub fn get_number_of_elements() -> u32 {
    get_element_by_id("elements")
        .unwrap()
        .dyn_into::<HtmlInputElement>()
        .map_err(|_| ())
        .unwrap()
        .value()
        .parse()
        .unwrap()
}

pub fn get_playback_speed_input() -> HtmlInputElement {
    get_element_by_id("speed")
        .unwrap()
        .dyn_into::<HtmlInputElement>()
        .map_err(|_| ())
        .unwrap()
}

pub fn get_playback_speed() -> Result<u32, ParseIntError> {
    get_playback_speed_input().value().parse()
}

pub fn get_dark_mode_input() -> HtmlInputElement {
    get_element_by_id("darkmodeinput")
        .unwrap()
        .dyn_into::<HtmlInputElement>()
        .map_err(|_| ())
        .unwrap()
}

pub fn get_dark_mode() -> bool {
    get_dark_mode_input().checked()
}

pub fn get_download_anchor() -> HtmlAnchorElement {
    get_element_by_id("download-anchor")
        .unwrap()
        .dyn_into::<HtmlAnchorElement>()
        .map_err(|_| ())
        .unwrap()
}

pub fn get_canvas_and_context(id: &str) -> (HtmlCanvasElement, CanvasRenderingContext2d) {
    let canvas = get_element_by_id(id)
        .unwrap()
        .dyn_into::<HtmlCanvasElement>()
        .map_err(|_| ())
        .unwrap();

    let context = canvas
        .get_context("2d")
        .unwrap()
        .unwrap()
        .dyn_into::<CanvasRenderingContext2d>()
        .unwrap();

    (canvas, context)
}

#[wasm_bindgen]
extern "C" {
    pub fn get_recording() -> bool;
}
